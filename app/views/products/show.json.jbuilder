json.extract! @product, :id, :title, :description, :image_url, :price, :category, :subcategory, :brand, :created_at, :updated_at
