class PageController < ApplicationController
 layout 'page'
  def index
  end
  
  def about
  end

  def contact
  end

  def faqs
  end

  def privacy
  end

  def terms
  end
  
end
